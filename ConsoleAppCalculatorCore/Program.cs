﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ConsoleAppCalculatorCore
{
    class Program
    {
        const string RegexBr = @"\(([1234567890\.\+\-\*\/^%]*)\)";      // Скобки
        const string RegexNum = @"[-]?\d+\.?\d*";                       // Числа
        const string RegexMulOp = @"[\*\/]";                            // Первоприоритетные операции
        const string RegexAddOp = @"[\+\-]";                            // Второприоритетные операции
        static void Main(string[] args)
        {
            Console.Title = "Тестовый калькулятор";
            Console.WriteLine("Напишите выражение для вычисления, используя +, -, *, /, скобки для группировки и числа. Для завершения работы введите пустую строку (Ввод).");
            string expr = string.Empty;
            do
            {
                expr = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(expr))
                {
                    var result = Parse(expr);
                    if (!double.IsPositiveInfinity(result))
                        Console.WriteLine(result.ToString(CultureInfo.InvariantCulture));
                }
            } while (!string.IsNullOrWhiteSpace(expr));
        }
        public static double Parse(string str)
        {
            // Парсинг скобок
            var matchSk = Regex.Match(str, RegexBr);
            if (matchSk.Groups.Count > 1)
            {
                string inner = matchSk.Groups[0].Value.Substring(1, matchSk.Groups[0].Value.Trim().Length - 2);
                string left = str.Substring(0, matchSk.Index);
                string right = str.Substring(matchSk.Index + matchSk.Length);

                return Parse(left + Parse(inner).ToString(CultureInfo.InvariantCulture) + right);
            }

            // Парсинг действий
            var matchMulOp = Regex.Match(str, $@"({RegexNum})\s?({RegexMulOp})\s?({RegexNum})\s?");
            var matchAddOp = Regex.Match(str, $@"({RegexNum})\s?({RegexAddOp})\s?({RegexNum})\s?");
            var match = matchMulOp.Groups.Count > 1 ? matchMulOp : matchAddOp.Groups.Count > 1 ? matchAddOp : null;
            if (match != null)
            {
                string left = str.Substring(0, match.Index);
                string right = str.Substring(match.Index + match.Length);
                return Parse(left + ParseAct(match).ToString(CultureInfo.InvariantCulture) + right);
            }

            // Парсинг числа
            try
            {
                return double.Parse(str, CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                Console.WriteLine($"Неверная входная строка '{str}'");
                return double.PositiveInfinity;
            }
        }
        private static double ParseAct(Match match)
        {
            double a = double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
            double b = double.Parse(match.Groups[3].Value, CultureInfo.InvariantCulture);

            switch (match.Groups[2].Value)
            {
                case "+": return a + b;
                case "-": return a - b;
                case "*": return a * b;
                case "/": return a / b;
                default: Console.WriteLine($"Неверная входная строка '{match.Value}'"); return double.PositiveInfinity;
            }
        }
    }
}
